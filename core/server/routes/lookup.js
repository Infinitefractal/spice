"use strict";
import Router from 'koa-router';
import jwt from 'koa-jwt';
import convert from 'koa-convert';
let router = new Router();
var lookup = require('../controllers/lookup');
var jwt_controller = require('../controllers/jwt');

router.prefix('/api/lookups');
router.use(convert(jwt({ secret: spice.config.jwt.secret})));
router.use(jwt_controller.check_user_exist);

router.get('/', lookup.list);
router.post('/', lookup.validate_insert, lookup.create);
router.get('/:lookup_id', lookup.get);
router.put('/:lookup_id', lookup.validate_update, lookup.update);
router.delete('/:lookup_id', lookup.delete);

module.exports = router;
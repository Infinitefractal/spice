'use strict';
import Router from 'koa-router';
import jwt from 'koa-jwt';
import convert from 'koa-convert';
var jwt_controller = require('../controllers/jwt');

let router = new Router();
router.prefix('/api/user_groups');
router.use(convert(jwt({ secret: spice.config.jwt.secret})));
router.use(jwt_controller.check_user_exist);

var userGroup = require('../controllers/userGroup');
router.post('/:entity_id/permissions/:permission_id', userGroup.add_permission);
router.delete('/:entity_id/permissions/:permission_id', userGroup.remove_permission);
router.get('/:entity_id/permissions', userGroup.get_permissions);
router.get('/:user_group_id', userGroup.get);
router.put('/:user_group_id', userGroup.update);
router.delete('/:user_group_id', userGroup.delete);
router.get('/', userGroup.list);
router.post('/', userGroup.validate_insert, userGroup.create);
module.exports = router;
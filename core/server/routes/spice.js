'use strict';
import Router from 'koa-router';

let router = new Router();

router.get(/^((?!\/api).)*$/, async function (ctx, next) {
    try{
        await ctx.render('index');
    }catch(e){
        console.log("Error", e);
    }
});
module.exports = router;
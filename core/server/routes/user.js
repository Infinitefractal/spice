'use strict';
import Router from 'koa-router';
let router = new Router();
import jwt from 'koa-jwt';
import convert from 'koa-convert';
var user = require('../controllers/user');
var jwt_controller = require('../controllers/jwt');

router.prefix('/api/users');
router.post('/', user.validate_insert, user.create);
router.post('/authenticate', user.validate_authentication, user.authenticate);
router.post('/password/email', user.validate_email, user.send_reset_password_email);
router.post('/password/reset', user.validate_password_reset, user.check_token);
router.use(convert(jwt({ secret: spice.config.jwt.secret}).unless({ path: [/^\/api\/users\/authenticate/] })));
router.use(jwt_controller.check_user_exist);

router.get('/', user.list);

router.post('/reset_password', user.validate_reset_password, user.reset_password);
router.get('/:user_id', user.get);
router.put('/:user_id', user.validate_update, user.update);
router.delete('/:user_id', user.delete);


module.exports = router;
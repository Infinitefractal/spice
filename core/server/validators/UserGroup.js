"use strict";
let Rules = spice.classes.validate.Rules;
import UserGroup from "../models/UserGroup";

Rules.prototype.usergroupShouldExist = function *(field, value, message){
    try{
        var group = new UserGroup();
        var group_found = yield group.get({id:value});
        console.log(group_found);
        return true;
    }catch(e){
        if(e.message == 'user_group does not exist'){
            this.validator.addError(field, 'rule', 'usergroupShouldExist', message || 'The usergroup must be created first');
            return false;
        }else{
            this.validator.addError(field, 'rule', 'usergroupShouldExist', 'Cannot Validate usergroup');
            return false;
        }
    }
}


Rules.prototype.userGroupIsUnique = function *(field, value, message){
    try{
        var group = new UserGroup();
        var group_found = yield group.get({id:value});
        console.log(group_found);

        this.validator.addError(field, 'rule', 'usergroupIsUnique', message || 'This usergroup has already been created');
        return false;
    }catch(e){
        if(e.message == 'user_group does not exist'){
            return true;
        }else{
            this.validator.addError(field, 'rule', 'userGroupIsUnique', 'Cannot Validate usergroup');
            return false;
        }
    }
}


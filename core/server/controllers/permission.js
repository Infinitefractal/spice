"use strict";
import  Permission from '../models/Permission';
var _ = require('lodash');
let utility = spice.classes.RestHelper;
import co from 'co';
const _slug = require('slug');


function slug(str) {
    return _slug(str, {
        replacement: '_',
        lower: true,
    })
}

var object = {
	get: async function(ctx, next){
		try{
			var permission = new Permission();
			ctx.body = utility.prepare_response(utility.SUCCESS,await permission.get({id:ctx.params.permission_id}));
		}catch(e){
			console.log(e)
			ctx.status = 400;
			ctx.body = utility.prepare_response(utility.FAILURE,e);
		}
	},

	list: async function(ctx, next){
		try{
			let query_params = {}
			_.defaults(query_params, ctx.query);
			var permission = new Permission();
			ctx.body = utility.prepare_collection(utility.SUCCESS,await permission.list(query_params));
		}catch(e){
			console.log(e.stack)
			ctx.status = 400;
			ctx.body = utility.prepare_response(utility.FAILURE,e);
		}
	},

	listByCategory: async function(ctx, next){
		try{
			let query_params = {category:ctx.query}
			_.defaults(query_params, ctx.query);
			var permission = new Permission();
			ctx.body = utility.prepare_response(utility.SUCCESS,await permission.by_category(query_params));
		}catch(e){
			console.log(e.stack)
			ctx.status = 400;
			ctx.body = utility.prepare_response(utility.FAILURE,e);
		}
	},

	update: async function(ctx, next){
		try{
			var permission = new Permission(ctx.request.body);
			ctx.body = utility.prepare_response(utility.SUCCESS,await permission.update({id:ctx.params.permission_id}));
		}catch(e){
			console.log(e)
			ctx.status = 400;
			ctx.body = utility.prepare_response(utility.FAILURE,e);
		}
	},

	create: async function(ctx, next){
		try{
			console.log('Running Create');
			var permission = new Permission(ctx.request.body);
			ctx.body = utility.prepare_response(utility.SUCCESS,await permission.create({id_prefix:'permission', id:slug(ctx.request.body.permission)}));
		}catch(e){
			console.log(e)
			ctx.status = 400;
			ctx.body = utility.prepare_response(utility.FAILURE,e);
		}
	},
	delete: async function(ctx, next){
		try{
			var permission = new Permission();
			ctx.body = utility.prepare_response(utility.SUCCESS,await permission.delete({id:ctx.params.permission_id, hard: true}));
		}catch(e){
			console.log(e)
			ctx.status = 400;
			ctx.body = utility.prepare_response(utility.FAILURE,e);
		}
	},

	validate_insert: async function(ctx, next){
		try{
			var rules = {
				permission: 'required'
			};
			var messages = {
				email: "Invalid Email",
				//password.between: "Invalid Password Length",
				//password.required: "Password is required"
			};
			var filters = {
				before: {
					//email: "lowercase|trim",
					//password: "trim"
				}
			}
			await co(ctx.validateBody(rules, messages, filters));
			if (ctx.validationErrors) {
				ctx.status = 422;
				ctx.body = ctx.validationErrors;
			} else {
				await next();
			}
		}catch(e){
			console.log(e.stack);
			ctx.status = 400;
			ctx.body = utility.prepare_response(utility.FAILURE, e);
		}

	},

	validate_update: async function(ctx, next){
        console.log(ctx.request.body)
        var rules = {
            //password: 'between:2,15',
            //email: 'email|emailIsUnique'
        };
        var messages = {
            //email: "Invalid Email"
        };
        var filters = {
            before: {
                //email: "lowercase|trim",
                //password: "trim"
            }
        }

        await co(ctx.validateBody(rules, messages, filters));

        if (ctx.validationErrors) {
            ctx.status = 422;
            ctx.body = ctx.validationErrors;
        } else {
            await next();
        }
	},
}

module.exports = object;
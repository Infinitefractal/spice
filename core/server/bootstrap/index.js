import Koa from "koa";
import Spice from "spice-js";
import dotenv from "dotenv";
const app = new Koa();
const spice_app = new Spice();
const convert = require('koa-convert');
const mode = process.env.MODE;

async function loader() {
    try {
        dotenv.config();
        await spice_app.init(app);
        app.server.listen(spice.config.spice.port);
        spice.logger.info("Spice started on port", spice.config.spice.port);
    } catch (e) {
        console.log(e.stack);
    }
}
loader();
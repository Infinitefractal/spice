"use strict";
var Crypto = spice.classes.Crypt;
let utility = spice.classes.RestHelper;
import UserGroup from './UserGroup';
let _ = require('lodash');
const props = {
    first_name:{type:"string", defaults:{read:""}},
    last_name:{type:"string", defaults:{read:""}},
    email:{type:"string", defaults:{read:""}},
    password:{type:"string"},
    salt:{type:"string"},
	reset_required:{type:"boolean", defaults:{read:false}},
    group:{type:"string", defaults:{read:""}},
    deleted:{type:"boolean"},
}
export default class User extends spice.classes.Model{
	constructor(args={}){
        super({connection:'default', props:props, args:args});
		this.type = 'user';
	}

	async authenticate(args){
		try{
			if(!args){
				args = {};
			}
			let query = ` deleted = false AND email='${args.email}' `;
			let results = await this.database.search(this.type, "", query||"", 1, 0);

			if(results.data.length > 0){
				let crypt = new Crypto();
				let password = await crypt.findHash(args.password, results.data[0].salt);
				if(password.hash == results.data[0].password){
					await this.run_hook(results.data[0], 'get', 'after');
					await this.do_serialize(results.data[0], 'read');
					return results.data[0];
				}
				throw new Error('unauthenticated');
			}
			throw new Error('unauthenticated');
		}catch(e){
			console.log(e.stack);
			throw e;
		}
	}

	async reset_password(args){

        try{
            let user = new User();
			console.log(args.email);
			if(!args){
				args = {};
			}
			let query = ` deleted = false AND email='${args.email}' `;
			let results = await this.database.search(this.type, "", query||"", 1, 0);
			let user_obj = results.data[0];
			if(results.data.length > 0) {
				let crypt = new Crypto();
				console.log("reset password user", user_obj);
				 if (args.old_password && user_obj) {
					let password = await crypt.findHash(args.old_password, user_obj.salt);
					if (password.hash != user_obj.password) {
						throw new Error("unauthorized");
					}
				} 

				//set new password
				let crypt_results = await crypt.encrypt(args.new_password);
				user_obj.salt = crypt_results.salt;
				user_obj.password = crypt_results.hash;
				user_obj.reset_required = "false";
				let update_user = new User(user_obj);
				let updated_user = await update_user.update({id: user_obj.id});
				console.log("Updated User", updated_user);
				return updated_user;
			}else{
				throw new Error('unauthorized');
			}

        }catch(e){
        	console.log(e.stack);
            throw new Error(e);
        }

	}


	getByEmail(args={}){
		return new Promise(async (resolve, reject)=>{
			try{
				_.defaults(args, {
					query:""
				});
				
				let query = args.query + `deleted = false AND email='${args.email}' `;
				let results = await this.database.search(this.type, "", query||"", 1, 0);
				if(results.data.length > 0){
					await this.run_hook(results.data[0], 'get', 'after');
					await this.do_serialize(results.data[0], 'read');
					resolve(results.data[0]);
				}else{
					reject('User not found for email provided');
				}
			}catch(e){
				console.log(e, e.stack);
				reject(e);
			}
		})
       
	}

	hooks(){
		return{
			create:{
				before:async (data)=>{
					try{
						let crypt = new Crypto();
						let crypt_results =  await crypt.encrypt(data.password);
						data.salt = crypt_results.salt;
						data.password = crypt_results.hash;
						return data;
					}catch(e){
						throw new Error(e);
					}
				},
				after:async (data)=>{
					try{
						//your code comes here
						return data;
					}catch(e){
						throw new Error(e);
					}

				}
			},
			get:{
				before:async (data)=>{
					try{
						//your code comes here
						return data;
					}catch(e){
						throw new Error(e);
					}
				},
				after:async (data)=>{
					try{
						//your code comes here
						return data;
					}catch(e){
						throw new Error(e);
					}
				}
			},
			update:{
				before:async (data)=>{
					try{
						//your code comes here
						
						return data;
					}catch(e){
						throw new Error(e);
					}
				},
				after:async (data)=>{
					try{
						//your code comes here
						return data;
					}catch(e){
						throw new Error(e);
					}
				}
			},
			delete:{
				before:async (data)=>{
					try{
						//your code comes here
						return data;
					}catch(e){
						throw new Error(e);
					}
				},
				after:async (data)=>{
					try{
						//your code comes here
						return data;
					}catch(e){
						throw new Error(e);
					}
				}
			}
		}
	}

	serializer(){
		return {
			read: {
				defaults: {

				},
				modifiers:[
					async (data)=>{
						try{
							if(!_.isArray(data)){
								data = Array.of(data);
							}
                            let ids = [];
                            _.each(data, result => {
                                ids = _.union(ids, [result.group]);
                            });
							let ug = await new UserGroup().getMulti({ids:ids});
							data = _.map(data, result =>{
								result.group = _.find(ug, g => g.id === result.group) || {}
								return result;
							})
							return data;
						}catch(e){
							throw new Error(e);
						}
					}
				],
				cleaners:[
					async (data)=>{

							try{
								if(_.isArray(data)){
									for(let i of data){
										delete i['deleted'];
										delete i['type'];
										delete i['salt'];
										delete i['password'];
									}
								}else{
									delete data['deleted'];
									delete data['type'];
									delete data['salt'];
									delete data['password'];
								}
								return data;
							}catch(e){
								console.log('cleaner error',e.stack);
								throw new Error(e);
							}
					}
				]
			},
			write:{
				defaults: {

				},
				modifiers:[
					async (data)=>{
						try{
							//your code comes here
							return data;
						}catch(e){
							throw new Error(e);
						}
					}
				],
				cleaners:[

				]
			}
		}
	}

}
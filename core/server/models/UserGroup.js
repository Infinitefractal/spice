"use strict";
import _ from 'lodash';
import Permission from './Permission'
let props = {
    name:{type:"string", defaults:{read:""}},
    created_at:{type:"string", defaults:{read:""}},
    updated_at:{type:"string", defaults:{read:""}},
    deleted:{type:"boolean", defaults:{read:""}},
    permissions:{type:"array", defaults:{read:[]}},
}

export default class UserGroup extends spice.classes.Model{
    constructor(args={}){
        super({connection:'default', props:props, args:args});
        this.type = 'user_group';
    }

    hooks(){
        return{
            create:{
                before:async (data)=>{
                    try{
                        //your code comes here
                        return data;
                    }catch(e){
                        throw new Error(e);
                    }
                },
                after:async (data)=>{
                    try{
                        //your code comes here
                        return data;
                    }catch(e){
                        throw new Error(e);
                    }

                }
            },
            get:{
                before:async (data)=>{
                    try{
                        //your code comes here
                        return data;
                    }catch(e){
                        throw new Error(e);
                    }
                },
                after:async (data)=>{
                    try{
                        //your code comes here
                        return data;
                    }catch(e){
                        throw new Error(e);
                    }
                }
            },
            update:{
                before:async (data)=>{
                    try{
                        //your code comes here
                        return data;
                    }catch(e){
                        throw new Error(e);
                    }
                },
                after:async (data)=>{
                    try{
                        //your code comes here
                        return data;
                    }catch(e){
                        throw new Error(e);
                    }
                }
            },
            delete:{
                before:async (data)=>{
                    try{
                        //your code comes here
                        return data;
                    }catch(e){
                        throw new Error(e);
                    }
                },
                after:async (data)=>{
                    try{
                        //your code comes here
                        return data;
                    }catch(e){
                        throw new Error(e);
                    }
                }
            }
        }
    }

    serializer(){
        return {
            read: {
                defaults: {

                },
                modifiers:[
                    async (results) => {
                        if(!_.isArray(results)) results = Array.of(results);
                        let permission_ids = [];
                        _.each(results, result => {
                            permission_ids = _.union(permission_ids, result.permissions);
                        });

                        let permissions = await new Permission().getMulti({ids:permission_ids});
                        _.each(results, result => {
                            if(!_.has(result, 'permissions')) {
                                result.permissions = [];
                                return;
                            }
                            result.permissions = _.map(result.permissions, pid => {
                                return _.find(permissions, p => p.id === pid);
                            });
                            result.permissions = _.reject(result.permissions, permission => permission === null || permission === undefined);
                        });
                        return results;
                    }
                ],
                cleaners:[
                    //this is the default cleaner is none is specifier. add cleaner to override the default
                    /*async (data)=>{
                        try{
                            if(_.isArray(data)){
                                for(let i of data){
                                    delete i['deleted'];
                                    delete i['type'];
                                }
                            }else{
                                delete data['deleted'];
                                delete data['type'];
                            }
                            return data;
                        }catch(e){
                            throw new Error(e);
                        }
                    }*/
                ]
            },
            write:{
                defaults: {

                },
                modifiers:[

                ],
                cleaners:[

                ]
            }
        }
    }
}
module.exports = {
    connections: {
        default: {
            enabled: true,
            database_type: process.env.DEFAULT_DATABASE_TYPE,
            bucket: process.env.DEFAULT_BUCKET,
            nodes: `couchbase://${process.env.DEFAULT_HOST}`,
            password : process.env.DEFAULT_PASSWORD,
            username: process.env.DEFAULT_USERNAME
        },
    }
}

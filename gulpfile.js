const gulp = require('gulp');
const watch = require('gulp-watch');
const babel = require('gulp-babel');
const clean = require('gulp-rimraf');
let paths = {};
paths.koa = './core/server/**/*.js';
paths.koa_routes = './core/server/routes/**/*.js';
paths.koa_config = './config/**/*.js';
const spawn = require('child_process').spawn;
let node;


gulp.task('clean_routes', gulp.series(function () {
    console.log("Clean all files in  routes build folder");
    return gulp.src("build/routes/*", {read: false}).pipe(clean());
}));

gulp.task('clean_config', gulp.series(function () {
    console.log("Clean all files in config build folder");
    return gulp.src("build/config/*", {read: false}).pipe(clean());
}));

gulp.task('build_koa', gulp.series(() => {
    console.log('Building Koa')
    return gulp.src('core/server/**')
        .pipe(babel({
            presets: ['es2015', 'stage-0']
        }))
        .pipe(gulp.dest('build'));
}));

gulp.task('build_routes', gulp.series('clean_routes', () => {
    console.log('Building Routes');
    return gulp.src('core/server/routes/**')
        .pipe(babel({
            presets: ['es2015', 'stage-0']
        }))
        .pipe(gulp.dest('build/routes'));
}));

gulp.task('build_config', gulp.series('clean_config', () => {
    console.log('Building  Config');
    return gulp.src('config/**')
        .pipe(babel({
            presets: ['es2015', 'stage-0']
        }))
        .pipe(gulp.dest('build/config'));
}));


gulp.task('server', gulp.series(function () {
    if (node) node.kill();
    node = spawn('node', ['build/index.js'], {stdio: 'inherit'})
    node.on('close', function (code) {
        if (code === 8) {
            gulp.log('Error detected, waiting for changes...');
        }
    });
}));


// Default task
gulp.task('default', gulp.series('build_koa', 'build_routes', 'build_config'));
//gulp.task('serve', gulp.series('server'));

gulp.task('watch', function(){
   //watch(paths.koa, gulp.series('build_koa', 'server'));
   gulp.watch(paths.koa_routes , gulp.series('build_routes', 'server')); 
   //gulp.watch(paths.koa_config, gulp.series('build_config', 'server'));
});
